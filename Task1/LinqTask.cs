﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task1.DoNotChange;

namespace Task1
{
    public static class LinqTask
    {
        public static IEnumerable<Customer> Linq1(IEnumerable<Customer> customers, decimal limit)
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Where(customer => customer.Orders
                    .Sum(order => order.Total) > limit)
                .Select(customer => customer);
            return result;
        }

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        )
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Select(customer => (customer, suppliers
                    .Where(supplier => supplier.Country == customer.Country && supplier.City == customer.City)
                    .Select(supplier => supplier)));
            return result;
        }

        public static IEnumerable<(Customer customer, IEnumerable<Supplier> suppliers)> Linq2UsingGroup(
            IEnumerable<Customer> customers,
            IEnumerable<Supplier> suppliers
        )
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            if (suppliers is null)
            {
                throw new ArgumentNullException(nameof(suppliers));
            }
            var result = customers
                .GroupJoin(suppliers,
                    customer => new { customer.Country, customer.City },
                    supplier => new { supplier.Country, supplier.City },
                    (customer, suppliers) => (customer, suppliers));
            return result;
        }

        public static IEnumerable<Customer> Linq3(IEnumerable<Customer> customers, decimal limit)
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Where(customer => customer.Orders.Length != 0 && customer.Orders
                    .Any(order => order.Total > limit))
                .Select(customer => customer);
            return result;
        }

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq4(
            IEnumerable<Customer> customers
        )
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Where(customer => customer.Orders.Length != 0)
                .Select(customer => (customer: customer, dateOfEntry: customer.Orders
                    .Min(order => order.OrderDate)));
            return result;
        }

        public static IEnumerable<(Customer customer, DateTime dateOfEntry)> Linq5(
            IEnumerable<Customer> customers
        )
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Where(customer => customer.Orders.Length != 0)
                .Select(customer => (customer: customer, dateOfEntry: customer.Orders
                    .Min(order => order.OrderDate)))
                .OrderBy(customerOrder => customerOrder.dateOfEntry.Year)
                .ThenBy(customerOrder => customerOrder.dateOfEntry.Month)
                .ThenByDescending(customerOrder => customerOrder.customer.Orders.Sum(order => order.Total))
                .ThenBy(customerOrder => customerOrder.customer.CompanyName);
            return result;
        }

        public static IEnumerable<Customer> Linq6(IEnumerable<Customer> customers)
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .Where(customer => customer.PostalCode.Any(ch => !char.IsDigit(ch)) || string.IsNullOrEmpty(customer.Region) || (!customer.Phone.Contains("(") && !customer.Phone.Contains(")")));
            return result;
        }

        public static IEnumerable<Linq7CategoryGroup> Linq7(IEnumerable<Product> products)
        {
            if (products is null)
            {
                throw new ArgumentNullException(nameof(products));
            }
            var result = products.GroupBy(product => product.Category)
                .Select(categoryGroup => new Linq7CategoryGroup
                {
                    Category = categoryGroup.Key,
                    UnitsInStockGroup = categoryGroup
                        .GroupBy(product => product.UnitsInStock)
                        .Select(unitsInStockGroup => new Linq7UnitsInStockGroup
                        {
                            UnitsInStock = unitsInStockGroup.Key,
                            Prices = unitsInStockGroup
                                .OrderBy(product => product.UnitPrice)
                                .Select(product => product.UnitPrice)
                        })
                });
            return result;
        }

        public static IEnumerable<(decimal category, IEnumerable<Product> products)> Linq8(
            IEnumerable<Product> products,
            decimal cheap,
            decimal middle,
            decimal expensive
        )
        {
            if (products is null)
            {
                throw new ArgumentNullException(nameof(products));
            }
            var result = products
                .GroupBy(product => product.UnitPrice <= cheap
                    ? cheap
                    : product.UnitPrice <= middle
                        ? middle
                        : expensive)
                .Select(productCategory => (category: productCategory.Key, product: productCategory.Select(p => p)));
            return result;
        }

        public static IEnumerable<(string city, int averageIncome, int averageIntensity)> Linq9(
            IEnumerable<Customer> customers
        )
        {
            if (customers is null)
            {
                throw new ArgumentNullException(nameof(customers));
            }
            var result = customers
                .GroupBy(customer => customer.City)
                .Select(keyCustomer => (keyCustomer.Key, Convert.ToInt32(keyCustomer.Average(c => c.Orders.Sum(order => order.Total))), Convert.ToInt32(keyCustomer.Average(order => order.Orders.Length))));
            return result;
        }

        public static string Linq10(IEnumerable<Supplier> suppliers)
        {
            if (suppliers is null)
            {
                throw new ArgumentNullException(nameof(suppliers));
            }
            var request = suppliers
                .OrderBy(supplier => supplier.Country.Length)
                .ThenBy(supplier => supplier.Country)
                .Select(supplier => supplier.Country)
                .Distinct();
            var result = string.Concat<string>(request.ToArray());
            return result;
        }
    }
}